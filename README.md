# wpsjs_demo

#### 介绍
wpsjs-开发wps插件，可以通过web的js调用本地的wps并做指定功能
官方接口文档：https://qn.cache.wpscdn.cn/encs/doc/office_v11/index.htm

#### 后端采用python-flask开发
技术采用python+flask，主要目的，
>从页面调起wps，插件自动化安装，并将文件和相关使用的参数全部传入
>监听wps插件上传文件，监听wps传入相关消息和事件
https://gitee.com/zuiyuewentian/wpsjs_demo/tree/master/server

#### 插件开发内容：
https://gitee.com/zuiyuewentian/wpsjs_demo/tree/master/plugin


#### 集成功能：
1.按钮功能开发
2.保存到本地
3.隐藏tab项目
4.插件和前端通信
5.插件和后端通信
6.web调用插件打开文件，传递参数
7.通过插件保存文件到服务器
8.插入书签内容
9.监听wps操作事件等

#### 常用wpsjs命令
准备开发环境
安装wps
安装Node.js

1.管理员权限(如果安装的是wps个人版，不需要管理员权限)启动命令行，通过npm全局安装wpsjs开发工具包:
安装命令： npm install -g wpsjs, 如果之前已经安装了，可以检查下wpsjs版本，更新wpsjs的命令为:npm update -g wpsjs

2.新建一个wps加载项，假设这个wps加载项取名为"HelloWps"。
输入命令： wpsjs create HelloWps, 会出现如下图的几个选项:

3通过上下方向键可以选择要创建的wps加载项的类型，如果选择“文字”，则创建的加载项会在wps文字程序中加载并运行，
同理选择“电子表格”，则会在wps表格中运行，这里假设我们选择的是“文字”，按Enter健确定。

4.选择示例代码的代码风格类型
wpsjs工具包提供了两种不同代码风格的示例，“无”代表示例代码中都是原生的js及html代码，没有集成vue\react等流行的前端框架。
"Vue"代表生成的示例代码集成了Vue相关的脚手架，在实际的项目中选用Vue基于示例代码可能更适合做工程化的开发，感兴趣的同学可以两种都尝试一下。
这里我们选择“无”，按Enter健确认。
确认后wpsjs工具包会在当前目录下生成一个HelloWps的文件夹，我们进入到此文件夹，可以看到HelloWps的相关代码已经生成：

备注：wpsjs工具包为示例代码中有一个package.json文件，这是node工具标准的配置文件，其中有一个依赖包为wps-jsapi,
这个依赖包是wps支持的全部接口的TypeScript描述，方便在vscode中敲代码时，提供代码联想功能，由于wps接口会跟随业务
需求不断更新，因此当发现代码联想对于有些接口不支持时，通过 npm update --save-dev wps-jsapi命令定期更新这个包。

#### 总结：
1.创建
wpsjs create 

2.加载项目
wpsjs join

3.本地调试
wpsjs debug

4.构建离线版
wpsjs build

5.构建发布版
wpsjs publish
