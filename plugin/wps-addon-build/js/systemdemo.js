function openOfficeFileFromSystemDemo(param){
    let jsonObj = (typeof(param)=='string' ? JSON.parse(param) : param)
    alert("从业务系统传过来的参数为：" + JSON.stringify(jsonObj))
    return {wps加载项项返回: jsonObj.filepath + ", 这个地址给的不正确"}
}

function InvokeFromSystemDemo(param){
    let jsonObj = (typeof(param)=='string' ? JSON.parse(param) : param)
    let handleInfo = jsonObj.Index
    let userName=jsonObj.CurrentUser
    let fileName=jsonObj.FileName
    let uploadPath=jsonObj.UploadPath
    let fileTitle=jsonObj.FileTitle
    let fileId=jsonObj.FileId
    let msgPath=jsonObj.MsgPath
    let markList=jsonObj.MarkList
    console.log(markList)
    wps.PluginStorage.setItem("UserName", userName)
    wps.PluginStorage.setItem("FileName", fileName)
    wps.PluginStorage.setItem("UploadPath", uploadPath)
    wps.PluginStorage.setItem("FileTitle", fileTitle)
    wps.PluginStorage.setItem("FileId", fileId)
    wps.PluginStorage.setItem("MsgPath", msgPath)
    wps.PluginStorage.setItem("MarkList", markList)
    switch (handleInfo){
        case "getDocumentName":{
            let docName = ""
            if (wps.WpsApplication().ActiveDocument){
                docName = wps.WpsApplication().ActiveDocument.Name
            }

            return {当前打开的文件名为:docName}
        }

        case "newDocument":{
            let newDocName=""
            let doc = wps.WpsApplication().Documents.Add()
            newDocName = doc.Name

            return {操作结果:"新建文档成功，文档名为：" + newDocName}
        }
        
        case "OpenFile":{
            let filePath = jsonObj.filepath
            wps.WpsApplication().Documents.OpenFromUrl(filePath)
            return {操作结果:"打开文件成功"}
        }
    }

    return {其它xxx:""}
}
