
//这个函数在整个wps加载项中是第一个执行的
function OnAddinLoad(ribbonUI) {
    if (typeof (wps.ribbonUI) != "object") {
        wps.ribbonUI = ribbonUI
    }

    if (typeof (wps.Enum) != "object") { // 如果没有内置枚举值
        wps.Enum = WPS_Enum
    }

    wps.ApiEvent.AddApiEventListener("DocumentBeforeClose", OnDocumentBeforeClose);
    wps.PluginStorage.setItem("UserName", "")//初始化当前用户是空值
    wps.PluginStorage.setItem("FileName", "")//初始化文件名
    wps.PluginStorage.setItem("UploadPath", "")//初始化上传路径
    wps.PluginStorage.setItem("FileTitle", "")//初始化文件名
    wps.PluginStorage.setItem("FileId", "")//初始化文件Id
    wps.PluginStorage.setItem("MsgPath", "")//初始化消息通知server地址
    wps.PluginStorage.setItem("MarkList", "")//初始化书签
    setTimeout(activeTab, 1000)
    return true
}

function activeTab() {
    //启动WPS程序后，默认显示的工具栏选项卡为ribbon.xml中某一tab
    if (wps.ribbonUI)
        wps.ribbonUI.ActivateTab('wpsAddinTab');
    wps.ribbonUI.InvalidateControl("lblEditUserValue")
}

var WebNotifycount = 0;
function OnAction(control) {
    const eleId = control.Id
    switch (eleId) {
        case "btndown":
            {
                const doc = wps.WpsApplication().ActiveDocument
                if (!doc) {
                    alert("当前没有打开任何文档")
                    return
                }
                OnBtnSaveAsLocalFile()
            }
            break;
        case "btnsave":
            {
                const doc = wps.WpsApplication().ActiveDocument
                if (!doc) {
                    alert("当前没有打开任何文档")
                    return
                }
                OnBtnSaveToServer()

            }
            break;
        case "btnclose":
            {
                const doc = wps.WpsApplication().ActiveDocument
                if (!doc) {
                    alert("当前没有打开任何文档")
                    return
                }
                OnBtnSaveClose()
            }
            break;
        case "btndata":
            var height = 250 * window.devicePixelRatio;
            var width = 400 * window.devicePixelRatio;
            wps.ShowDialog(GetUrlPath() + "/ui/mark.html", "书签管理", width, height, false);
            //wps.ShowDialog(GetUrlPath() + "/ui/dialog.html", "这是一个对话框网页", 400 * window.devicePixelRatio, 400 * window.devicePixelRatio, false)
            break
        default:
            break
    }
    return true
}

function GetUrlPath() {
    let e = document.location.toString()
    return -1 != (e = decodeURI(e)).indexOf("/") && (e = e.substring(0, e.lastIndexOf("/"))), e
}

function GetImage(control) {
    const eleId = control.Id
    switch (eleId) {
        case "btnsave":
            return "images/save.svg"
        case "btndata":
            return "images/data.svg"
        case "btndown":
            return "images/down.svg"
        case "btnclose":
            return "images/out.svg"
        default:
            ;
    }
    return "images/save.svg"
}

function OnGetEnabled(control) {
    const eleId = control.Id
    switch (eleId) {
        case "btnShowMsg":
            return true
            break
        case "btnShowDialog":
            {
                let bFlag = wps.PluginStorage.getItem("EnableFlag")
                return bFlag
                break
            }
        case "btnShowTaskPane":
            {
                let bFlag = wps.PluginStorage.getItem("EnableFlag")
                return bFlag
                break
            }
        default:
            break
    }
    return true
}

function OnGetVisible(control) {
    return true
}

function OnGetVisibleF(control) {
    return false
}

function OnGetLabel(control) {
    const eleId = control.Id
    switch (eleId) {
        case "lblEditState":
            {
                return "打开时间"
                break
            }
        case "lblEditStateValue":
            {
                return Thistime()
                break
            }
        case "lblEditUser":
            {
                return "操作用户"
                break
            }
        case "lblEditUserValue":
            {
                let user = wps.PluginStorage.getItem("UserName")
                return user
                break
            }
    }
    return ""
}

//取当前日期
function Thistime() { //当前日期
    var date = new Date();
    var mytime = date.toLocaleTimeString('chinese', { hour12: false });
    var time = mytime;
    return time;
}


/**
 *  判断当前文档是否是系统传过来的文件，判断当前文档名称是否与传过来的名称一直
 */
function CheckIfSysDoc() {
    var doc = wps.WpsApplication().ActiveDocument;
    if (!doc)
        return false;

    docName = wps.WpsApplication().ActiveDocument.Name
    let fileName = wps.PluginStorage.getItem("FileName")
    if (fileName == docName)
        return true
    else
        return false
}

function OnNewDocumentApiEvent(doc) {
    alert("新建文件事件响应，取文件名: " + doc.Name)
}



/**
 *  执行另存为本地文件操作
 */
function OnBtnSaveAsLocalFile() {

    //检测是否有文档正在处理
    var l_doc = wps.WpsApplication().ActiveDocument;
    if (!l_doc) {
        alert("WPS当前没有可操作文档！");
        return;
    }

    // 设置WPS文档对话框 2 FileDialogType:=msoFileDialogSaveAs
    var l_ksoFileDialog = wps.WpsApplication().FileDialog(2);
    l_ksoFileDialog.InitialFileName = l_doc.Name; //文档名称

    if (l_ksoFileDialog.Show() == -1) { // -1 代表确认按钮
        l_ksoFileDialog.Execute(); //会触发保存文档的监听函数
        wps.ribbonUI.Invalidate(); //刷新Ribbon的状态
    };
}


/**
 *  执行关闭后保存
 */
function OnBtnSaveClose() {
    var l_doc = wps.WpsApplication().ActiveDocument;
    if (!l_doc) {
        alert("空文档不能保存！");
        return;
    }

    //非系统文档，不能上传到系统
    if (CheckIfSysDoc() == false) {
        alert("非系统打开的文档，不能直接上传到系统！");
        return;
    }
    /**
       * 参数定义：OAAsist.UploadFile(name, path, url, field,  "OnSuccess", "OnFail")
       * 上传一个文件到远程服务器。
       * name：为上传后的文件名称；
       * path：是文件绝对路径；
       * url：为上传地址；
       * field：为请求中name的值；
       * 最后两个参数为回调函数名称；
       */
    var l_uploadPath = wps.PluginStorage.getItem("UploadPath"); // 文件上载路径
    if (l_uploadPath == "") {
        wps.alert("系统未传入文件上载路径，不能执行上传操作！");
        return;
    }

    if (!wps.confirm("先保存文档，并开始上传到系统后台，请确认？")) {
        return;
    }


    var l_FieldName = wps.PluginStorage.getItem("FileTitle"); //上载到后台的业务方自定义的字段名称
    if (l_FieldName == "") {
        l_FieldName = "file"; // 默认为‘file’
    }

    var l_UploadName = wps.PluginStorage.getItem("FileName"); //设置sys传入的文件名称参数
    if (l_UploadName == "") {
        l_UploadName = l_doc.Name; //默认文件名称就是当前文件编辑名称
    }

    var l_DocPath = l_doc.FullName; // 文件所在路径

    //对于本地磁盘文件上传OA，先用Save方法保存后，再上传
    //设置用户保存按钮标志，避免出现禁止OA文件保存的干扰信息
    //wps.PluginStorage.setItem(constStrEnum.OADocUserSave, EnumDocSaveFlag.OADocSave);
    if (l_doc.Path == "") { //对于不落地文档，文档路径为空
        l_doc.SaveAs2(wps.Env.GetTempPath() + "/" + l_doc.Name, undefined, undefined, undefined, false);
    } else {
        l_doc.Save();
    }
    //执行一次保存方法
    //设置用户保存按钮标志
    //wps.PluginStorage.setItem(constStrEnum.OADocUserSave, EnumDocSaveFlag.NoneOADocSave);
    //落地文档，调用UploadFile方法上传到OA后台
    l_DocPath = l_doc.FullName;
    try {
        //调用OA助手的上传方法
        UploadFile(l_UploadName, l_DocPath, l_uploadPath, l_FieldName, OnUploadToSaveSuccess, OnUploadToSaveFail);
    } catch (err) {
        console.log(err)
        UpdateToServerWeb(1)
        alert("上传文件失败！请检查系统上传参数及网络环境！");
    }
}

/**
 * 调用文件上传到服务端时，
 * @param {*} resp 
 */
function OnUploadToSaveSuccess(resp) {
    console.log("成功上传服务端后的回调：" + resp)
    console.log(resp)
    var l_doc = wps.WpsApplication().ActiveDocument;
    alert("上传成功！");
    l_doc.Close(-1); //保存文档
    UpdateToServerWeb(2)
}

function OnUploadToSaveFail(resp) {
    console.log(resp)
    UpdateToServerWeb(1)
    alert("文件上传失败！");
}



//保存到后台服务器
function OnBtnSaveToServer() {
    var l_doc = wps.WpsApplication().ActiveDocument;
    if (!l_doc) {
        alert("空文档不能保存！");
        return;
    }

    //非系统文档，不能上传到系统
    if (CheckIfSysDoc() == false) {
        alert("非系统打开的文档，不能直接上传到系统！");
        return;
    }

    /**
     * 参数定义：OAAsist.UploadFile(name, path, url, field,  "OnSuccess", "OnFail")
     * 上传一个文件到远程服务器。
     * name：为上传后的文件名称；
     * path：是文件绝对路径；
     * url：为上传地址；
     * field：为请求中name的值；
     * 最后两个参数为回调函数名称；
     */
    var l_uploadPath = wps.PluginStorage.getItem("UploadPath"); // 文件上载路径
    if (l_uploadPath == "") {
        wps.alert("系统未传入文件上载路径，不能执行上传操作！");
        return;
    }

    if (!wps.confirm("先保存文档，并开始上传到系统后台，请确认？")) {
        return;
    }


    var l_FieldName = wps.PluginStorage.getItem("FileTitle"); //上载到后台的业务方自定义的字段名称
    if (l_FieldName == "") {
        l_FieldName = "file"; // 默认为‘file’
    }

    var l_UploadName = wps.PluginStorage.getItem("FileName"); //设置sys传入的文件名称参数
    if (l_UploadName == "") {
        l_UploadName = l_doc.Name; //默认文件名称就是当前文件编辑名称
    }

    var l_DocPath = l_doc.FullName; // 文件所在路径

    //对于本地磁盘文件上传OA，先用Save方法保存后，再上传
    //设置用户保存按钮标志，避免出现禁止OA文件保存的干扰信息
    //wps.PluginStorage.setItem(constStrEnum.OADocUserSave, EnumDocSaveFlag.OADocSave);
    if (l_doc.Path == "") { //对于不落地文档，文档路径为空
        l_doc.SaveAs2(wps.Env.GetTempPath() + "/" + l_doc.Name, undefined, undefined, undefined, false);
    } else {
        l_doc.Save();
    }
    //执行一次保存方法
    //设置用户保存按钮标志
    //wps.PluginStorage.setItem(constStrEnum.OADocUserSave, EnumDocSaveFlag.NoneOADocSave);
    //落地文档，调用UploadFile方法上传到OA后台
    l_DocPath = l_doc.FullName;
    try {
        //调用OA助手的上传方法
        UploadFile(l_UploadName, l_DocPath, l_uploadPath, l_FieldName, OnUploadToServerSuccess, OnUploadToServerFail);
    } catch (err) {
        console.log(err)
        UpdateToServerWeb(1)
        alert("上传文件失败！请检查系统上传参数及网络环境！");
    }

}


/**
 * 调用文件上传到服务端成功时，
 * @param {*} resp 
 */
function OnUploadToServerSuccess(resp) {
    console.log("成功上传服务端后的回调：" + resp)
    console.log(resp)
    var l_doc = wps.WpsApplication().ActiveDocument;

    if (wps.confirm("文件上传成功！继续编辑请确认，取消关闭文档。") == false) {
        if (l_doc) {
            console.log("OnUploadToServerSuccess: before Close");
            l_doc.Close(-1); //保存文档后关闭
            console.log("OnUploadToServerSuccess: after Close");
            UpdateToServerWeb(2)
        }
    } else {
        UpdateToServerWeb(0)
    }

}

function OnUploadToServerFail(resp) {
    console.log(resp)
    UpdateToServerWeb(1)
    alert("文件上传失败！");
}


/**
 * WPS上传文件到服务端（业务系统可根据实际情况进行修改，为了兼容中文，服务端约定用UTF-8编码格式）
 * @param {*} strFileName 上传到服务端的文件名称（包含文件后缀）
 * @param {*} strPath 上传文件的文件路径（文件在操作系统的绝对路径）
 * @param {*} uploadPath 上传文件的服务端地址
 * @param {*} strFieldName 业务调用方自定义的一些内容可通过此字段传递，默认赋值'file'
 * @param {*} OnSuccess 上传成功后的回调
 * @param {*} OnFail 上传失败后的回调
 */
function UploadFile(strFileName, strPath, uploadPath, strFieldName, OnSuccess, OnFail) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', uploadPath);

    var fileData = wps.FileSystem.readAsBinaryString(strPath);
    var data = new FakeFormData();
    if (strFieldName == "" || typeof strFieldName == "undefined") {//如果业务方没定义，默认设置为'file'
        strFieldName = 'file';
    }
    data.append(strFieldName, {
        name: utf16ToUtf8(strFileName), //主要是考虑中文名的情况，服务端约定用utf-8来解码。
        type: "application/octet-stream",
        getAsBinary: function () {
            return fileData;
        }
    });
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200)
                OnSuccess(xhr.response)
            else
                OnFail(xhr.response);
        }
    };
    xhr.setRequestHeader("Cache-Control", "no-cache");
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    if (data.fake) {
        xhr.setRequestHeader("Content-Type", "multipart/form-data; boundary=" + data.boundary);
        var arr = StringToUint8Array(data.toString());
        xhr.send(arr);
    } else {
        xhr.send(data);
    }
}

//UTF-16转UTF-8
function utf16ToUtf8(s) {
    if (!s) {
        return;
    }
    var i, code, ret = [],
        len = s.length;
    for (i = 0; i < len; i++) {
        code = s.charCodeAt(i);
        if (code > 0x0 && code <= 0x7f) {
            //单字节
            //UTF-16 0000 - 007F
            //UTF-8  0xxxxxxx
            ret.push(s.charAt(i));
        } else if (code >= 0x80 && code <= 0x7ff) {
            //双字节
            //UTF-16 0080 - 07FF
            //UTF-8  110xxxxx 10xxxxxx
            ret.push(
                //110xxxxx
                String.fromCharCode(0xc0 | ((code >> 6) & 0x1f)),
                //10xxxxxx
                String.fromCharCode(0x80 | (code & 0x3f))
            );
        } else if (code >= 0x800 && code <= 0xffff) {
            //三字节
            //UTF-16 0800 - FFFF
            //UTF-8  1110xxxx 10xxxxxx 10xxxxxx
            ret.push(
                //1110xxxx
                String.fromCharCode(0xe0 | ((code >> 12) & 0xf)),
                //10xxxxxx
                String.fromCharCode(0x80 | ((code >> 6) & 0x3f)),
                //10xxxxxx
                String.fromCharCode(0x80 | (code & 0x3f))
            );
        }
    }

    return ret.join('');

}

function StringToUint8Array(string) {
    var binLen, buffer, chars, i, _i;
    binLen = string.length;
    buffer = new ArrayBuffer(binLen);
    chars = new Uint8Array(buffer);
    for (var i = 0; i < binLen; ++i) {
        chars[i] = String.prototype.charCodeAt.call(string, i);
    }
    return buffer;
}


/**
 * 更新到服务器状态
 * @param {*} state 0-保存成功 1-保存失败 2-文件关闭  
 */
function UpdateToServer(state) {

    let p_Url = wps.PluginStorage.getItem("MsgPath")
    if (p_Url == '') {
        return
    }
    let docId = wps.PluginStorage.getItem("FileId")
    if (docId == '') {
        return
    }
    var formData = {
        "docId": docId,
        "state": state
    };
    $.ajax({
        url: p_Url, //URL + '/document/stateMonitor',
        async: false,
        data: formData,
        method: "post",
        dataType: 'json',
        success: function (response) {
            if (response == "success") {
                console.log(response);
            }
        },
        error: function (response) {
            console.log(response);
        }
    });
}

/**
 * 更新到web页面状态
 * @param {*} state 0-保存成功 1-保存失败 2-文件关闭  
 */
function UpdateToServerWeb(state) {
    let docId = wps.PluginStorage.getItem("FileId")
    if (docId == '') {
        return
    }
    var formData = {
        "docId": docId,
        "state": state
    };
    wps.OAAssist.WebNotify(JSON.stringify(formData), true)
}


//文档保存前关闭事件
/**
 * 作用
 */
function OnDocumentBeforeClose() {
    console.log('OnDocumentBeforeClose');
    var doc = wps.WpsApplication().ActiveDocument;
    var l_fullName = doc.FullName;
    var l_bIsOADoc = false;
    l_bIsOADoc = CheckIfSysDoc(); //判断是否系统文档要关闭
    if (l_bIsOADoc == false) { // 非系统文档不做处理
        return;
    }

    if (doc.Saved == false) { //如果OA文档关闭前，有未保存的数据
        if (wps.confirm("系统文件有改动，是否提交后关闭？" + "\n" + "确认后请按上传按钮执行上传操作。取消则继续关闭文档。")) {
            wps.ApiEvent.Cancel = true;
            return;
        }
    }

    // 有未保存的数据，确认无需保存直接关闭
    doc.Close(); 
    wps.FileSystem.Remove(l_fullName);
}
