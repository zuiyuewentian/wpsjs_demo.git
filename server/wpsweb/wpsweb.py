from os import environ
from flask import Flask
from flask import render_template,request
import os

app = Flask(__name__)
Save_Path = './static/upload/'

@app.route('/', methods=("GET", "POST"))
def test():
    return render_template('test.html')

@app.route('/Upload', methods=("GET", "POST"))
def Upload():
	if request.method == 'POST':
		try:
			file = request.files['test']
			savePath = os.path.join(Save_Path, file.filename)
			file.save(savePath)
			return {'msg': 'success'}, 200, {"Content-Type": "text/html;charset=utf-8"}
		except Exception as e:
			print(str(e))
			return False
	else:
		return False

@app.route('/ClientMsg',methods=("GET", "POST"))
def LisMessage():
	if request.method == 'POST':
		docId = request.form['docId']
		state = request.form['state']
		return "success"
	else:
		return "fail"


if __name__ == '__main__':
    HOST = environ.get('SERVER_HOST', '0.0.0.0')
    try:
        PORT = int(environ.get('SERVER_PORT', '5000'))
    except ValueError:
        PORT = 5002
    app.run(HOST, PORT)
